##############################################################################################################################################
# This example needs two runs due to a timing issue setting a policy to allow the sender to out a subscripotion filter on the receipient logs.
##############################################################################################################################################
terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-cloud-watch-log-exporter-simple"
    }
  }
}

provider "aws" {
  region  = "eu-west-1"
  profile = "cloud-watch-exporter-sender"
  alias   = "sender"
}

provider "aws" {
  region  = "eu-west-1"
  profile = "cloud-watch-exporter-recipient"
  alias   = "recipient"
}

data "aws_caller_identity" "sender" {
  provider = aws.sender
}

module "cloud_watch_log_exporter_recipient" {
  source = "../../"

  name = "test-module-simple"

  is_log_recipient = true
  sender_accounts  = {
    sender = data.aws_caller_identity.sender.account_id
  }

  s3_delivery_firehose_buffer_interval = 60


  providers = {
    aws           = aws.recipient
    aws.us_east_1 = aws.recipient
  }
}

module "cloud_watch_log_exporter_sender" {
  source = "../../"

  name = "test-module-simple"

  is_log_sender = true

  cloudwatch_log_group_name = aws_cloudwatch_log_group.test.name
  log_destination_arn       = values(module.cloud_watch_log_exporter_recipient.created_recipient_log_destinations)[0]

  cloudwatch_subscription_distribution = "Random"

  providers = {
    aws           = aws.sender
    aws.us_east_1 = aws.sender
  }
}

resource "aws_cloudwatch_log_group" "test" {
  name              = "simple-test-log"
  retention_in_days = 1

  provider = aws.sender
}

resource "aws_cloudwatch_log_stream" "test" {
  name           = "simple-test-stream"
  log_group_name = aws_cloudwatch_log_group.test.name

  provider = aws.sender
}
