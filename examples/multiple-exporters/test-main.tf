##############################################################################################################################################
# This example needs two runs due to a timing issue setting a policy to allow the sender to out a subscripotion filter on the receipient logs.
##############################################################################################################################################
terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-cloud-watch-log-exporter-multiple-exporters"
    }
  }
}

provider "aws" {
  region  = "eu-west-1"
  profile = "cloud-watch-exporter-recipient"
  alias   = "recipient"
}

provider "aws" {
  region  = "eu-west-1"
  profile = "cloud-watch-exporter-sender-1"
  alias   = "sender_1"
}

provider "aws" {
  region  = "eu-west-1"
  profile = "cloud-watch-exporter-sender-2"
  alias   = "sender_2"
}

data "aws_caller_identity" "sender_1" {
  provider = aws.sender_1
}

data "aws_caller_identity" "sender_2" {
  provider = aws.sender_2
}

module "cloud_watch_log_exporter_recipient" {
  source = "../../"

  name = "test-multiple-recipient"

  is_log_recipient = true

  sender_accounts = {
    sender_1 = data.aws_caller_identity.sender_1.account_id
    sender_2 = data.aws_caller_identity.sender_2.account_id
  }

  s3_delivery_firehose_buffer_interval = 60
  s3_bucket_force_destroy              = true

  providers = {
    aws           = aws.recipient
    aws.us_east_1 = aws.recipient
  }
}

module "cloud_watch_log_exporter_sender_1" {
  source = "../../"

  name = "test-multiple-sender-1"

  is_log_sender = true

  cloudwatch_log_group_name = aws_cloudwatch_log_group.sender_1.name
  log_destination_arn       = values(module.cloud_watch_log_exporter_recipient.created_recipient_log_destinations)[0]

  providers = {
    aws           = aws.sender_1
    aws.us_east_1 = aws.sender_1
  }
}

resource "aws_cloudwatch_log_group" "sender_1" {
  name              = "multiple-test-log-1"
  retention_in_days = 1

  provider = aws.sender_1
}

resource "aws_cloudwatch_log_stream" "sender_1" {
  name           = "multiple-test-log-1"
  log_group_name = aws_cloudwatch_log_group.sender_1.name

  provider = aws.sender_1
}

module "cloud_watch_log_exporter_sender_2" {
  source = "../../"

  name = "test-multiple-sender-2"

  is_log_sender = true

  cloudwatch_log_group_name = aws_cloudwatch_log_group.sender_2.name
  log_destination_arn       = values(module.cloud_watch_log_exporter_recipient.created_recipient_log_destinations)[1]

  providers = {
    aws           = aws.sender_2
    aws.us_east_1 = aws.sender_2
  }
}

resource "aws_cloudwatch_log_group" "sender_2" {
  name              = "multiple-test-log-2"
  retention_in_days = 1

  provider = aws.sender_2
}

resource "aws_cloudwatch_log_stream" "sender_2" {
  name           = "multiple-test-log-2"
  log_group_name = aws_cloudwatch_log_group.sender_2.name

  provider = aws.sender_2
}
