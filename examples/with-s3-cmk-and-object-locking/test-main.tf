##############################################################################################################################################
# This example needs a pre-run in order to create the bucket before setting up the log export:
#                        make init plan ARGS="-target module.bucket"; make apply
# This example needs two runs due to a timing issue setting a policy to allow the sender to out a subscripotion filter on the receipient logs.
##############################################################################################################################################
terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-cloud-watch-log-exporter-with-s3-cmk-and-object-locking"
    }
  }
}

variable "aws_region" {
  default = "eu-west-1"
}

provider "aws" {
  region  = var.aws_region
  profile = "cloud-watch-exporter-sender"
  alias   = "sender"
}

provider "aws" {
  region  = var.aws_region
  profile = "cloud-watch-exporter-recipient"
  alias   = "recipient"
}

data "aws_caller_identity" "sender" {
  provider = aws.sender
}

data "aws_caller_identity" "recipient" {
  provider = aws.recipient
}

module "cloud_watch_log_exporter_recipient" {
  source = "../../"

  name = "test-module-cmk-objlock"

  is_log_recipient = true
  sender_accounts  = {
    sender = data.aws_caller_identity.sender.account_id
  }

  encrypt_kinesis_stream                 = false
  encrypt_s3_delivery_firehose_error_log = true

  create_s3_bucket = false

  s3_bucket_name                       = var.bucket_name
  s3_object_prefix                     = var.bucket_object_prefix
  s3_delivery_firehose_buffer_interval = 60

  s3_kms_key_arn = module.bucket_cmk.key_arn

  providers = {
    aws           = aws.recipient
    aws.us_east_1 = aws.recipient
  }
}

module "cloud_watch_log_exporter_sender" {
  source = "../../"

  name = "test-module-cmk-objlock"

  is_log_sender = true

  cloudwatch_log_group_name = aws_cloudwatch_log_group.test.name
  log_destination_arn       = values(module.cloud_watch_log_exporter_recipient.created_recipient_log_destinations)[0]

  cloudwatch_subscription_distribution = "Random"

  providers = {
    aws           = aws.sender
    aws.us_east_1 = aws.sender
  }
}

resource "aws_cloudwatch_log_group" "test" {
  name              = "with-cmk-and-object-locking-test-log"
  retention_in_days = 1

  kms_key_id = module.logs_cmk.key_arn

  provider = aws.sender
}

resource "aws_cloudwatch_log_stream" "test" {
  name           = "test-stream"
  log_group_name = aws_cloudwatch_log_group.test.name

  provider = aws.sender
}

module "logs_cmk" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/kms-key.git?ref=v1.0.3"

  key_name        = "test-log"
  key_description = "CMK for CloudWatch logs group test-log"

  key_policy = [{
    effect     = "Allow"
    principals = [{
      type        = "Service",
      identifiers = ["logs.${var.aws_region}.amazonaws.com"]
    }]
    actions    = [
      "kms:Encrypt*",
      "kms:Decrypt*",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:Describe*"
    ]
    resources  = ["*"]
    condition  = []
  }]

  providers = {
    aws = aws.recipient
  }
}

variable "bucket_name" {
  default = "osdevex-tf-modules-aws-cloud-watch-log-exporter-cmk-objlock"
}

variable "bucket_object_prefix" {
  default = "test-prefix"
}

module "bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket        = var.bucket_name
  region        = var.aws_region
  acl           = "log-delivery-write"
  force_destroy = true

  versioning = {
    enabled = true
  }

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = module.bucket_cmk.key_arn
      }
    }
  }

  object_lock_configuration = {
    object_lock_enabled = "Enabled"
    rule                = {
      default_retention = {
        mode = "GOVERNANCE"
        days = 1
      }
    }
  }

  providers = {
    aws = aws.recipient
  }
}

module "bucket_cmk" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/kms-key.git?ref=v1.0.3"

  key_name        = var.bucket_name
  key_description = "CMK for S3 bucket ${var.bucket_name}"

  key_policy = [
    {
      principals = [
        {
          type        = "AWS",
          identifiers = [module.cloud_watch_log_exporter_recipient.recipient_firehose_iam_role_arn]
        },
      ]

      effect    = "Allow"
      actions   = [
        "kms:Decrypt",
        "kms:GenerateDataKey",
      ]
      resources = ["*"]

      condition = [
        {
          test     = "StringEquals"
          variable = "kms:ViaService"
          values   = ["s3.${var.aws_region}.amazonaws.com"]
        },
        {
          test     = "StringLike"
          variable = "kms:EncryptionContext:aws:s3:arn"
          values   = ["arn:aws:s3:::${var.bucket_name}/${var.bucket_object_prefix}*"]
        },
      ]
    },
  ]

  providers = {
    aws = aws.recipient
  }
}
