terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-cloud-watch-log-exporter-only-create-role"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

module "cloud_watch_log_exporter_recipient" {
  source = "../../"

  name = "test-module-only-create-iam-role"

  create_recipient_firehose_iam_role = true

  providers = {
    aws           = aws
    aws.us_east_1 = aws
  }
}

output "recipient_firehose_role_name" {
  value = module.cloud_watch_log_exporter_recipient.recipient_firehose_iam_role_name
}

output "recipient_firehose_role_arn" {
  value = module.cloud_watch_log_exporter_recipient.recipient_firehose_iam_role_arn
}
