locals {
  firehose_names                   = [for account_name in keys(var.sender_accounts): "${account_name}-${local.recipient_name}-fireh"]
  # Name given to single resources related to firehose, like IAM role, or CMK
  firehose_single_resource_name    = "${local.recipient_name}-fireh"
  # Use provided role, or use name variable if module is simply creating a role, or the firehose stream name after which the created role for a recipient is named
  recipient_firehose_iam_role_name = local.firehose_iam_role_provided  ? join("", data.aws_iam_role.recipient_firehose_provided.*.name) : (local.only_create_firehose_iam_role ? var.name : local.firehose_single_resource_name)
  recipient_firehose_iam_role_arn  = local.create_firehose_iam_role ? module.recipient_firehose_iam_role.iam_role_arn_firehose_delivery : var.recipient_firehose_iam_role_arn

  bucket_object_prefixes = formatlist("%s/%s", keys(var.sender_accounts), var.s3_object_prefix != "" ? "${var.s3_object_prefix}/${var.name}" : var.name)
  #  bucket_object_prefix = var.s3_object_prefix != "" ? "${var.s3_object_prefix}/${var.name}" : var.name
}

resource "aws_kinesis_firehose_delivery_stream" "recipient_firehose_delivery_stream" {
  count = local.is_log_recipient ? local.recipient_count : 0

  name        = local.firehose_names[count.index]
  destination = "s3"

  kinesis_source_configuration {
    kinesis_stream_arn = aws_kinesis_stream.recipient_data_stream[count.index].arn
    role_arn           = local.recipient_firehose_iam_role_arn
  }

  s3_configuration {
    role_arn           = local.recipient_firehose_iam_role_arn
    bucket_arn         = local.bucket_arn
    prefix             = format("%s/", local.bucket_object_prefixes[count.index]) # unlike in S3 bucket resources this prefix needs an ending /
    buffer_size        = var.s3_delivery_firehose_buffer_size
    buffer_interval    = var.s3_delivery_firehose_buffer_interval
    compression_format = var.s3_delivery_firehose_compression_format

    # Since the source of the data from a firehose perspective is a kinesis data stream, data will never be persisted by firehose,
    # and that means no KMS key is needed.
    # See "Server-Side Encryption with Kinesis Data Streams as the Data Source" on https://docs.aws.amazon.com/firehose/latest/dev/encryption.html
    #    kms_key_arn        = ""

    cloudwatch_logging_options {
      enabled         = true
      log_group_name  = aws_cloudwatch_log_group.recipient_firehose_errors[count.index].name
      log_stream_name = aws_cloudwatch_log_stream.recipient_firehose_errors[count.index].name
    }
  }

  tags = var.tags
}

resource "aws_cloudwatch_log_group" "recipient_firehose_errors" {
  count = local.is_log_recipient ? local.recipient_count : 0

  name = "/aws/kinesisfirehose/${local.firehose_names[count.index]}"

  retention_in_days = var.recipient_firehose_error_log_retention_days
}

resource "aws_cloudwatch_log_stream" "recipient_firehose_errors" {
  count = local.is_log_recipient ? local.recipient_count : 0

  log_group_name = aws_cloudwatch_log_group.recipient_firehose_errors[count.index].name
  name           = "S3Delivery"
}

module "recipient_firehose_errors_cmk" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/kms-key.git?ref=v1.0.3"

  project     = var.project
  environment = var.environment
  tags        = var.tags

  key_name        = "${local.firehose_single_resource_name}-error-log"
  key_description = "CMK for Kinesis Firehose Streams ${local.firehose_single_resource_name} error log"

  create_key = local.is_log_recipient && var.encrypt_s3_delivery_firehose_error_log

  key_policy = [{
    effect     = "Allow"
    principals = [{
      type        = "Service",
      identifiers = ["logs.${local.aws_region}.amazonaws.com"]
    }]
    actions    = [
      "kms:Encrypt*",
      "kms:Decrypt*",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:Describe*"
    ]
    resources  = ["*"]
    condition  = []
  }]
}
