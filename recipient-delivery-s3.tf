locals {
  create_bucket = local.is_log_recipient && var.create_s3_bucket
  bucket_name   = var.s3_bucket_name != "" ? var.s3_bucket_name : "${local.recipient_name}-s3"
  bucket_arn    = local.create_bucket ? join("", aws_s3_bucket.created.*.arn) : join("", data.aws_s3_bucket.existing.*.arn)
}

resource "aws_s3_bucket" "created" {
  count = local.create_bucket ? 1 : 0

  bucket = local.bucket_name
  acl    = "log-delivery-write"

  force_destroy = var.s3_bucket_force_destroy

  tags = var.tags
}

resource "aws_s3_bucket_public_access_block" "created" {
  count = local.create_bucket && var.s3_bucket_block_public_access ? 1 : 0

  bucket = join("", aws_s3_bucket.created.*.id)

  block_public_acls   = true
  block_public_policy = true
}

data "aws_s3_bucket" "existing" {
  count = local.is_log_recipient && !local.create_bucket ? 1 : 0

  bucket = local.bucket_name
}
