resource "aws_iam_role" "recipient_firehose_role" {
  count = var.create_role ? 1 : 0

  name               = var.iam_role_name
  assume_role_policy = join("", data.aws_iam_policy_document.recipient_firehose_assume_role.*.json)

  tags = var.tags
}

data "aws_iam_policy_document" "recipient_firehose_assume_role" {
  count = var.create_role ? 1 : 0

  statement {
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["firehose.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}
