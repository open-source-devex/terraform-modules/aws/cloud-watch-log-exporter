variable "create_role" {
  description = "Whether to create the resources"
  type        = bool
  default     = true
}

variable "tags" {
  description = "Tags to be added to resources that support it"
  type        = map(string)
  default     = {}
}

variable "iam_role_name" {
  description = "The name for the IAM role"
  type        = string
}
