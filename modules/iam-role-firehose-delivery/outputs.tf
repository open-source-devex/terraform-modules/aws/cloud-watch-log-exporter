output "iam_role_arn_firehose_delivery" {
  value = join("", aws_iam_role.recipient_firehose_role.*.arn)
}

output "iam_role_name_firehose_delivery" {
  value = join("", aws_iam_role.recipient_firehose_role.*.name)
}
