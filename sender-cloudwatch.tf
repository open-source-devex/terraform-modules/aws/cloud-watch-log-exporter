locals {
  cloudwatch_subscription_name = var.cloudwatch_subscription_name != "" ? var.cloudwatch_subscription_name : local.sender_name
}

resource "aws_cloudwatch_log_subscription_filter" "sender" {
  count = local.is_log_sender ? 1 : 0

  name            = local.cloudwatch_subscription_name
  log_group_name  = var.cloudwatch_log_group_name
  filter_pattern  = var.cloudwatch_subscription_filter_pattern
  destination_arn = var.log_destination_arn
  distribution    = var.cloudwatch_subscription_distribution

  provider = aws.us_east_1
}
