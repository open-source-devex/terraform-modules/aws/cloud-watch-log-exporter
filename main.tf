provider "aws" {
  alias = "us_east_1"
}

locals {
  aws_region = var.aws_region != "" ? var.aws_region : data.aws_region.current.name

  recipient_count = length(var.sender_accounts)

  recipient_name = "r-${var.name}"
  sender_name    = "s-${var.name}"

  is_log_recipient = var.enabled && var.is_log_recipient
  is_log_sender    = var.enabled && var.is_log_sender
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}
