locals {
  firehose_iam_role_provided    = var.recipient_firehose_iam_role_arn != ""
  create_firehose_iam_role      = (local.is_log_recipient && !local.firehose_iam_role_provided) || (var.enabled && var.create_recipient_firehose_iam_role)
  only_create_firehose_iam_role = !local.is_log_recipient && !local.is_log_sender && var.create_recipient_firehose_iam_role
}

module "recipient_firehose_iam_role" {
  source = "./modules/iam-role-firehose-delivery"

  create_role   = local.create_firehose_iam_role
  iam_role_name = local.recipient_firehose_iam_role_name

  tags = var.tags
}

# This data source makes sure that the provided role exists
data "aws_iam_role" "recipient_firehose_provided" {
  count = local.firehose_iam_role_provided ? 1 : 0

  name = replace(var.recipient_firehose_iam_role_arn, "/^.*:role\\//", "")
}

resource "aws_iam_role_policy_attachment" "recipient_firehose_role" {
  count = local.is_log_recipient ? 1 : 0

  role       = local.recipient_firehose_iam_role_name
  policy_arn = join("", aws_iam_policy.recipient_firehose_role.*.arn)
}

resource "aws_iam_policy" "recipient_firehose_role" {
  count = local.is_log_recipient ? 1 : 0

  name   = local.recipient_firehose_iam_role_name
  policy = join("", data.aws_iam_policy_document.recipient_firehose_role_policy.*.json)
}

# For a full list of required permissions see "Grant Kinesis Data Firehose Access to an Amazon S3 Destination" in
# https://docs.aws.amazon.com/firehose/latest/dev/controlling-access.html#using-iam-s3
data "aws_iam_policy_document" "recipient_firehose_role_policy" {
  count = local.is_log_recipient ? 1 : 0

  # Allow firehose to log errors
  statement {
    sid       = "AllowFirehoseToLogErrorsToCloudWatch"
    effect    = "Allow"
    actions   = ["logs:PutLogEvents"]
    resources = formatlist("arn:aws:logs:${local.aws_region}:${data.aws_caller_identity.current.account_id}:log-group:%s:log-stream:*", aws_cloudwatch_log_group.recipient_firehose_errors.*.name)

  }

  # Allow firehose to use KMS key configured on CloudWatch log group to encrypt errors events
  dynamic "statement" {
    # build list with single element if var.encrypt_s3_delivery_firehose_error_log
    for_each = [for element in [1] : module.recipient_firehose_errors_cmk.key_arn if var.encrypt_s3_delivery_firehose_error_log]

    content {
      sid       = "AllowFirehoseToEncryptCloudWatchLogsForErrors"
      effect    = "Allow"
      actions   = [
        "kms:GenerateDataKey*",
      ]
      resources = [statement.value]
    }
  }

  # Allow firehose to read from Kinesis data stream
  statement {
    sid       = "AllowFirehoseToGetDataStreamRecords"
    effect    = "Allow"
    actions   = [
      "kinesis:DescribeStream",
      "kinesis:GetShardIterator",
      "kinesis:GetRecords"
    ]
    resources = formatlist("arn:aws:kinesis:${local.aws_region}:${data.aws_caller_identity.current.account_id}:stream/%s", local.kinesis_stream_names)
  }

  # Allow firehose to use KMS key configured on source Kinesis data stream to decrypt log events
  dynamic "statement" {
    # build list with single element if var.encrypt_kinesis_stream
    for_each = [for element in [1] : module.logs_to_data_stream_cmk.key_arn if var.encrypt_kinesis_stream]

    content {
      sid       = "AllowFirehoseToDecryptKinesisDataStreamRecords"
      effect    = "Allow"
      actions   = [
        "kms:Decrypt",
      ]
      resources = [statement.value]
    }
  }

  # Allow firehose to write to target S3 bucket
  statement {
    sid       = "AllowFirehoseToWriteToS3Bucket"
    effect    = "Allow"
    actions   = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject"
    ]
    resources = concat(["arn:aws:s3:::${local.bucket_name}"], formatlist("arn:aws:s3:::${local.bucket_name}/%s*", local.bucket_object_prefixes))
  }

  # Allow firehose to use KMS key configured on target S3 bucket to encrypt objects
  dynamic "statement" {
    # build list with single element if var.kms_key_arn is provided
    for_each = [for element in [1] : var.s3_kms_key_arn if var.s3_kms_key_arn != ""]

    content {
      sid       = "AllowFirehoseToEncryptAndDecryptS3BucketObjects"
      effect    = "Allow"
      actions   = [
        "kms:Decrypt",
        "kms:GenerateDataKey",
      ]
      resources = [statement.value]
      condition {
        test     = "StringEquals"
        variable = "kms:ViaService"
        values   = ["s3.${local.aws_region}.amazonaws.com"]
      }
      condition {
        test     = "StringLike"
        variable = "kms:EncryptionContext:aws:s3:arn"
        values   = formatlist("arn:aws:s3:::${local.bucket_name}/%s*", local.bucket_object_prefixes)
      }
    }
  }
}
