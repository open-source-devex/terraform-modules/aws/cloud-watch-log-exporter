locals {
  # This enabled destruction of log destinations without triggerign a "Error in function call"
  # othewise, if the account names received as variables are used, on destroy there will be a different number of keys and values
  recipient_log_destination_account_names = [for name in aws_cloudwatch_log_destination.recipient_log_destination.*.name : replace(name, "/-.*$/", "")]
}

# sender accounts use this destination set subscription filters on their log groups
output "created_recipient_log_destinations" {
  value = zipmap(local.recipient_log_destination_account_names, aws_cloudwatch_log_destination.recipient_log_destination.*.arn)
}

output "created_recipient_kinesis_stream_arns" {
  value = zipmap(local.recipient_log_destination_account_names, aws_kinesis_stream.recipient_data_stream.*.arn)
}

output "created_recipient_s3_bucket" {
  value = join("", aws_s3_bucket.created.*.arn)
}

output "recipient_firehose_iam_role_arn" {
  value = local.recipient_firehose_iam_role_arn
}

output "recipient_firehose_iam_role_name" {
  value = local.recipient_firehose_iam_role_name
}
