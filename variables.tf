variable project {
  type    = string
  default = "prj"
}

variable environment {
  type    = string
  default = "env"
}

variable "name" {
  description = "The name of this export, used to name resources created in this context"
  type        = string
}

variable "tags" {
  description = "Tags to be added to resources that support it"
  type        = map(string)
  default     = {}
}

variable "enabled" {
  description = "Whether to create any resource within this module"
  type        = bool
  default     = true
}

variable "aws_region" {
  description = "The region where the recipient S3 bucket is created, will default to the region of the provider when lefts empty."
  default     = ""
}

variable "is_log_sender" {
  description = "Whether the account is the log sender"
  type        = bool
  default     = false
}

variable "is_log_recipient" {
  description = "Whether the account is the log recipient"
  type        = bool
  default     = false
}

variable "create_recipient_firehose_iam_role" {
  description = "Whether to create the recipeient fole for delivery of logs by firehose. When both is_log_sender and is_log_recipient are set to false and create_recipent_firehose_iam_role is set to true, the IAM role is still created and the value of name is used for the role name."
  type        = bool
  default     = false
}

variable "recipient_firehose_iam_role_arn" {
  description = "The ARN of the role assumed by firehose to deliver logs to the s3 bucket"
  type        = string
  default     = ""
}

variable "sender_accounts" {
  description = "A list of ids of the sender accounts, required when is_log_recipient"
  type        = map(string)
  default     = {}
}

variable "log_destination_arn" {
  description = "The ARN of the log recipient (created by this module when is_log_recipient), required when is_log_sender"
  type        = string
  default     = ""
}

variable "cloudwatch_log_group_name" {
  description = "The name of the CloudWatch log group the sender wants to export, required when is_log_sender"
  type        = string
  default     = ""
}

variable "cloudwatch_subscription_name" {
  description = "The name for the CloudWatch logs subscription. Leave empty to assign default name"
  type        = string
  default     = ""
}

variable "cloudwatch_subscription_distribution" {
  description = "The method used by the sender to distribute log data to the destination. By default log data is grouped by log stream, but the grouping can be set to random for a more even distribution. Valid values are Random and ByLogStream."
  type        = string
  default     = "ByLogStream"
}

variable "cloudwatch_subscription_filter_pattern" {
  description = "The pattern used to filter log events to export. Empty string matches all events. See https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/FilterAndPatternSyntax.html"
  type        = string
  default     = ""
}

variable "recipient_iam_role_arn" {
  description = "The ARN of the role to grant CloudWatch permission to deliver logs to Kinesis stream. Required when create_kinesis_stream is disabled and is_log_recipient."
  type        = string
  default     = ""
}

variable "create_kinesis_stream" {
  description = "Whether to create a Kinesis stream to receive the log events"
  type        = bool
  default     = true
}

variable "kinesis_stream_shard_count" {
  description = "The number of shards that the stream will use."
  type        = number
  default     = 1
}

variable "kinesis_stream_retention_period" {
  description = "Number of hours data records are accessible after they are added to the stream. Minimum 24, maximum 168."
  type        = number
  default     = 24
}

variable "encrypt_kinesis_stream" {
  description = "Whether to configure the Kinesis data stream with a CMK key."
  type        = bool
  default     = false
}

variable "create_s3_bucket" {
  description = "Whether to create an S3 bucket to hold the log events"
  type        = bool
  default     = true
}

variable "s3_bucket_name" {
  description = "The name of the S3 bucket where log events will be stored. Leave empty to assign default name"
  type        = string
  default     = ""
}

variable "s3_object_prefix" {
  description = "A prefix to be added before the name to the path of the log event objects. Example: /prefix/name/..."
  type        = string
  default     = ""
}

variable "s3_bucket_force_destroy" {
  description = "Allow terraform to destroy the bucket created by this module when it wants to remove or recrete it"
  type        = bool
  default     = false
}

variable "s3_bucket_block_public_access" {
  description = "Whether to place a block on public access to the created S3 bucket"
  type        = bool
  default     = true
}

variable "s3_kms_key_arn" {
  description = "The ARN of the CMK used together with the S3 bucket. This key will be used by Kinesis and S3 to encrypt data at rest"
  type        = string
  default     = ""
}

variable "s3_delivery_firehose_buffer_size" {
  description = "Buffer data to the specified size, in MBs, before delivering it to the s3 bucket. The recommended setting shuold be a value greater than the amount of data typically ingested into the delivery stream in 10 seconds. For example, if the stream typically ingests data at 1 MB/sec set SizeInMBs to be 10 MB or higher."
  type        = number
  default     = 10
}

variable "s3_delivery_firehose_buffer_interval" {
  description = "Buffer data for the specified period of time, in seconds, before delivering it to the s3 bucket."
  type        = number
  default     = 300
}

variable "s3_delivery_firehose_compression_format" {
  description = "The compression format. Supported values are UNCOMPRESSED, GZIP, ZIP & Snappy."
  type        = string
  default     = "GZIP"
}

variable "encrypt_s3_delivery_firehose_error_log" {
  description = "Whether to configure the CloudWatch log group for Kinesis firehose stream errors with a CMK key."
  type        = bool
  default     = false
}

variable "recipient_firehose_error_log_retention_days" {
  description = "How many days should firehost delivery to s3 errors be retained in the log group"
  type        = number
  default     = 30
}
