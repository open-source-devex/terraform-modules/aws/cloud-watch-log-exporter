#################################################################################
# Implementation of CloudWatch resources
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CreateDestination.html
#################################################################################
locals {
  recipient_iam_role_arn = var.create_kinesis_stream ? join("", aws_iam_role.logs_to_data_stream.*.arn) : var.recipient_iam_role_arn
  log_destination_names  = [for account_name in keys(var.sender_accounts): "${account_name}-${local.recipient_name}-log-dest"]
}

resource "aws_cloudwatch_log_destination" "recipient_log_destination" {
  count = local.is_log_recipient ? local.recipient_count : 0

  name       = local.log_destination_names[count.index]
  role_arn   = local.recipient_iam_role_arn
  target_arn = aws_kinesis_stream.recipient_data_stream[count.index].arn

  provider = aws.us_east_1
}

resource "aws_cloudwatch_log_destination_policy" "recipient_log_destination_policy" {
  count = local.is_log_recipient ? local.recipient_count : 0

  destination_name = aws_cloudwatch_log_destination.recipient_log_destination[count.index].name
  access_policy    = data.aws_iam_policy_document.recipient_log_destination_policy[count.index].json

  provider = aws.us_east_1
}

data "aws_iam_policy_document" "recipient_log_destination_policy" {
  count = local.is_log_recipient ? local.recipient_count : 0

  statement {
    effect  = "Allow"
    principals {
      type        = "AWS"
      identifiers = [values(var.sender_accounts)[count.index]]
    }
    actions = ["logs:PutSubscriptionFilter"]

    resources = distinct([
      "arn:aws:logs:${local.aws_region}:${data.aws_caller_identity.current.account_id}:destination:${local.log_destination_names[count.index]}",
      # Hardcoded: region must be us-east-1 becasue that's the destination is being created
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:destination:${local.log_destination_names[count.index]}",
    ])
  }
}

