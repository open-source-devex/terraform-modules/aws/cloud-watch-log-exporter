#################################################################################
# Implementation of Kinesis resources
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CreateDestination.html
#################################################################################
locals {
  create_kinesis_data_stream = local.is_log_recipient && var.create_kinesis_stream

  kinesis_stream_names         = [for account_name in keys(var.sender_accounts): "${account_name}-${local.recipient_name}-kdata"]
  # Name given to single resources related to data stream, like IAM role, or CMK
  kinesis_single_resource_name = "${local.recipient_name}-kdata"
}

resource "aws_kinesis_stream" "recipient_data_stream" {
  count = local.create_kinesis_data_stream ? local.recipient_count : 0

  name             = local.kinesis_stream_names[count.index]
  shard_count      = var.kinesis_stream_shard_count
  retention_period = var.kinesis_stream_retention_period

  encryption_type = var.encrypt_kinesis_stream ? "KMS" : "NONE"
  kms_key_id      = module.logs_to_data_stream_cmk.key_arn

  shard_level_metrics = [
    "IncomingBytes",
    "IncomingRecords",
    "IteratorAgeMilliseconds",
    "OutgoingBytes",
    "OutgoingRecords",
    "ReadProvisionedThroughputExceeded",
    "WriteProvisionedThroughputExceeded",
  ]

  tags = var.tags
}

resource "aws_iam_role" "logs_to_data_stream" {
  count = local.create_kinesis_data_stream ? 1 : 0

  name               = local.kinesis_single_resource_name
  assume_role_policy = join("", data.aws_iam_policy_document.logs_to_data_stream_assume_role_policy.*.json)

  tags = var.tags
}

data "aws_iam_policy_document" "logs_to_data_stream_assume_role_policy" {
  count = local.create_kinesis_data_stream ? 1 : 0

  statement {
    effect  = "Allow"
    principals {
      type = "Service"

      # Hardcoded us-east-1 because Route53 runs there, and so do CloudWatch log groups for DNS query logs
      identifiers = distinct([
        "logs.us-east-1.amazonaws.com",
        "logs.${local.aws_region}.amazonaws.com",
      ])
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy_attachment" "logs_to_data_stream" {
  count = local.create_kinesis_data_stream ? 1 : 0

  role       = join("", aws_iam_role.logs_to_data_stream.*.name)
  policy_arn = join("", aws_iam_policy.logs_to_data_stream.*.arn)
}

resource "aws_iam_policy" "logs_to_data_stream" {
  count = local.create_kinesis_data_stream ? 1 : 0

  name   = local.kinesis_single_resource_name
  policy = join("", data.aws_iam_policy_document.logs_to_data_stream_policy.*.json)
}

data "aws_iam_policy_document" "logs_to_data_stream_policy" {
  count = local.create_kinesis_data_stream ? 1 : 0

  # Allow AWS logs to put records on kinesis data stream
  statement {
    effect    = "Allow"
    actions   = ["kinesis:PutRecord"]
    resources = formatlist("arn:aws:kinesis:${local.aws_region}:${data.aws_caller_identity.current.account_id}:stream/%s", local.kinesis_stream_names)
  }

  # Allow AWS logs to encrypt log records with kinesis data stream CMK
  dynamic "statement" {
    # build list with single element if var.encrypt_kinesis_stream
    for_each = [for element in [1] : module.logs_to_data_stream_cmk.key_arn if var.encrypt_kinesis_stream]

    content {
      effect    = "Allow"
      actions   = ["kms:GenerateDataKey"]
      resources = [statement.value]
    }
  }

  # Allow AWS logs to pass on its role
  statement {
    effect    = "Allow"
    actions   = ["iam:PassRole"]
    resources = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${local.kinesis_single_resource_name}"]
  }
}

module "logs_to_data_stream_cmk" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/kms-key.git?ref=v1.0.3"

  key_name        = local.kinesis_single_resource_name
  key_description = "CMK for Kinesis Data Streams ${local.kinesis_single_resource_name}"

  create_key = local.is_log_recipient && var.encrypt_kinesis_stream

  key_policy = [
    {
      sid        = "AllowKinesisDataStreamToEncryptWithCMK"
      effect     = "Allow"
      principals = [
        {
          type        = "AWS",
          identifiers = [local.recipient_iam_role_arn]
        },
      ]
      actions    = [
        "kms:GenerateDataKey",
      ]
      resources  = ["*"]
      condition  = []
    },
    {
      sid        = "AllowKinesisFirehoseToDecryptWithCMK"
      effect     = "Allow"
      principals = [
        {
          type        = "AWS",
          identifiers = [local.recipient_firehose_iam_role_arn]
        },
      ]
      actions    = [
        "kms:Decrypt",
      ]
      resources  = ["*"]
      condition  = []
    },
  ]

  tags = var.tags
}
