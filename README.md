# cloudwatch-log-exporter

Terraform module to export CloudWatch logs to Kinesis or S3.
This module implements the process described here https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CrossAccountSubscriptions.html

The module when instantiated by the recipient (ie. the consumer of the logs) it creates a log destination that will receive log events (that pass a specific filter).
That log destination is then coupled with a Kinesis data stream that holds the events.
The Kinesis data stream is then coupled with a Kinesis firehose stream that delivers the events to the S3 bucket.
The Kinesis firehose stream only holds events in memory, therefore it does not need a KMS key for encryption at rest.
The final destination if the log events is an S3 bucket.

The module when instantiated by the sender (ie. the producer of the logs) adds a subscription filter to the CloudWatch log group that uses the log destination created by the recipient.

All required resources for the streaming of logs from CloudWatch to S3 bucket are created by this module, including, optionally, also the target S3 bucket.
However, when having the S3 bucket created bu this module it won't have a KMS key configured for encryption at rest.
The only place where data can (and eventually will) be persisted is the Kinesis data stream.
Therefore this module does create a CMK for the Kinesis data stream.

For setups where multiple log exporters are configured, it can be useful to re-use the IAM role assumed by Kinesis Firehose because this role might be needed for setting up KMS
 key policies. In these cases, the module can be used as in example [only create role](./examples/only-create-role/test-main.tf), to only create the IAM role.

## CMK on S3 bucket

The IAM permissions on the rule assumed by firehose as described in the [documentation from AWS](https://docs.aws.amazon.com/firehose/latest/dev/controlling-access.html#using-iam-s3) is not enough.
The same permissions that are placed on the IAM role need to also be placed on the CMK policy, otherwise firehose won't be able to deliver to the bucket.
See example [with-s3-cmk-and-object-locking](./examples/with-s3-cmk-and-object-locking/test-main.tf) for a working setup.

## AWS Regions

This module expects a AWS provider with alias "us_east_1" in order to manipulate resources in the "us-east-1" region.
This is required for Route53 DNS query logs since Route53 hosted zones live in "us-east-1" and so do the CloudWatch log groups that they log to.
For any other type of log exporting either than Route53 DNS query logs, feel free to set the aliased provider to the default provider.
This way the module will create all resources in the default region, instead of creating log destinations in "us-east-1".
The [simple example](./exmaples/simple/test-main.tf) in this module shows how to use the module within a single region.
